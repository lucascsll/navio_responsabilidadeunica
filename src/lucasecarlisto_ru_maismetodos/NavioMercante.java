/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucasecarlisto_ru_maismetodos;

/**
 *
 * @author Lucas
 */
public class NavioMercante extends Navio {
    
    
   private float carga;
  private float capacidade;


public NavioMercante(float carga , float capacidade, String name,int qtdTripulantes ){
    super(name , qtdTripulantes);
    this.carga=carga;
    this.capacidade = capacidade;
}

 
  
  
  public float getCarga(){
      return this.carga=carga;
  }
  
  public float getCapacidade(){
      return this.capacidade=capacidade;
  }
    
}
